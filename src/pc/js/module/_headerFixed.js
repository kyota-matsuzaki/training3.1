export default function headerFixed() {
    'use strict';

    let $headerDistance = $('#is-header').offset().top;
    let $isHeader = $('#is-header');
    let $replace = $('.replace');
    const CLASSNAME =  {
            FIXED: 'fixed',
            BLOCK: 'block',
        };

    $(window).on('scroll', () => {
        let $windowHeight = $(window).scrollTop();
        if($windowHeight > $headerDistance){
            $isHeader.addClass(CLASSNAME.FIXED);
            $replace.addClass(CLASSNAME.BLOCK);
        }else {
            $isHeader.removeClass(CLASSNAME.FIXED);
            $replace.removeClass(CLASSNAME.BLOCK);
        }
    });

    //when header is fixed it's to responsive
    let $windowWidth = $(window).width();
    let $header = $(".header");

    let headerResponsive = () => {
        if ($windowWidth < 1160) {
            $header.css("width", 1100);
        }
        else {
            $header.css("width", "100%");
        }
    };
    headerResponsive();
}
