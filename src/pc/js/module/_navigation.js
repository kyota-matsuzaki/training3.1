export default function navigation() {

    'use strict';
    const $NAVIGATION_LINK = $('.navigation-link');

    //when click nav, disply scroll
    $NAVIGATION_LINK.click((e) => {
        e.preventDefault();
        let speed = 400;
        let href= $(e.currentTarget).attr("href");
        let target = $(href == "#" || href == "" ? 'html' : href);
        let position = target.offset().top - 95;
        $('html, body').animate({scrollTop:position}, speed, 'swing');
        return false;
    });

    // add and remove "current" class to elements at certain scroll positions.
    let topHeight = $("#js-top").height();
    let featureHeight = $("#js-feature").outerHeight();
    let serviceHeight = $(".l-main-service").outerHeight();
    let newsHeight = $("#js-news").height();

    let controller = new ScrollMagic.Controller();
    let top = new ScrollMagic.Scene({triggerElement: "#js-top", triggerHook: "onCenter",
        offset: -98
        });
        top.addTo(controller);
        top.setClassToggle("#js-nav-top", "is-current");
        top.duration(topHeight);
    let feature = new ScrollMagic.Scene({triggerElement: "#js-feature", triggerHook: "onCenter",
        offset: -98
        });
        feature.addTo(controller);
        feature.setClassToggle("#js-nav-feature", "is-current");
        feature.duration(featureHeight);
    let service = new ScrollMagic.Scene({triggerElement: ".l-main-service", triggerHook: "onCenter",
        offset: -98
        });
        service.addTo(controller);
        service.setClassToggle("#js-nav-service", "is-current");
        service.duration(serviceHeight);
    let news = new ScrollMagic.Scene({triggerElement: "#js-news", triggerHook: "onCenter",
        offset: -98
        });
        news.addTo(controller);
        news.setClassToggle("#js-nav-news", "is-current");
        news.duration(newsHeight);
}