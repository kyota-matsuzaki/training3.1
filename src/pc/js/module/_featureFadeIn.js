export default function featureFadeIn() {
    'use strict';

    const IS_ACTIVE = "is-active";
    let controller = new ScrollMagic.Controller();

    let fadeIn = new ScrollMagic.Scene({
    triggerElement: ".js-feature",
    triggerHook: "onEnter"
    });
    fadeIn.addTo(controller);
    fadeIn.setClassToggle(".js-feature", IS_ACTIVE);
}
