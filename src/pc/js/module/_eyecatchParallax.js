export default function eyecatchParallax() {
    'use strict';

    let controller = new ScrollMagic.Controller();
    let img1 = $('#js-parallax-1');
    let img2 = $('#js-parallax-2');
    let img3 = $('#js-parallax-3');
    let topHeight = $("#js-top").height();

    let parallax = new ScrollMagic.Scene({
    triggerElement: "#js-top",
    duration: topHeight
    });
    parallax.on('progress', function(e){
        let amountOfScroll1 = e.progress * -80 + "px";
        let amountOfScroll2 = e.progress * -150 + "px";
        let amountOfScroll3 = e.progress * -250 + "px";
        img1.css("transform", "translateY(" + amountOfScroll1 + ")");
        img2.css("transform", "translateY(" + amountOfScroll2 + ")");
        img3.css("transform", "translateY(" + amountOfScroll3 + ")");
    });
    parallax.addTo(controller);

}
